package com.skillup.automation.signUp.positive;

import com.skillup.automation.TestRunner;
import org.testng.annotations.Test;

import static com.skillup.automation.utils.RandomEmailString.generateString;
import static com.skillup.automation.utils.RandomEmailString.getRandomEmail;

public class PositiveSignUpTest extends TestRunner {
    private static final String EXPECTED_SIGN_UP_URL = "https://www.linkedin.com/start/join";

    @Test
    public void positiveSignUpTest() {

        String email = getRandomEmail(10);
        String password = generateString(12);

        signUpPage
                .open()
                .enterFirstName("firstName")
                .enterLastName("lastName")
                .enterEmail(email)
                .enterPassword(password)
                .clickJoinButton();

        onBoardingPage.assertIsOnPage(EXPECTED_SIGN_UP_URL);


    }
}
