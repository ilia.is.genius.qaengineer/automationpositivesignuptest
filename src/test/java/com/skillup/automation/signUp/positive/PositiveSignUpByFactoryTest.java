package com.skillup.automation.signUp.positive;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.SignUpPageByFactory;
import org.testng.annotations.Test;

import static com.skillup.automation.utils.RandomEmailString.generateString;
import static com.skillup.automation.utils.RandomEmailString.getRandomEmail;

public class PositiveSignUpByFactoryTest extends TestRunner {

    private static final String EXPECTED_LOGIN_URL = "https://www.linkedin.com/start/join";

    @Test
    public void positiveSignUpTest() {

        SignUpPageByFactory signUpPage = new SignUpPageByFactory(driver);

        String email = getRandomEmail(10);
        String password = generateString(8);

        signUpPage
                .open()
                .enterFirstName("Hello")
                .enterLastName("Hi")
                .enterEmail(email)
                .enterPassword(password)
                .clickSignUpButton();

        onBoardingPage.assertIsOnPage(EXPECTED_LOGIN_URL);


    }

}