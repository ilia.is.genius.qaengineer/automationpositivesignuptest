package com.skillup.automation.signUp.negative;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.SignUpPageByFactory;
import org.testng.annotations.Test;

import static com.skillup.automation.utils.RandomEmailString.generateString;


public class NegativeSignUpByFactoryTest extends TestRunner {

    private static final String EXPECTED_ERROR_MESSAGE = "Please enter your email address";


    @Test
    public void test() {
        SignUpPageByFactory signUpPage = new SignUpPageByFactory(driver);

        String password = generateString(10);

        signUpPage
                .open()
                .enterFirstName("First Name")
                .enterLastName("Last Name")
                .enterEmail("")
                .enterPassword(password)
                .clickSignUpButton()
                .assertEmailErrorMessage(EXPECTED_ERROR_MESSAGE);
    }
}
