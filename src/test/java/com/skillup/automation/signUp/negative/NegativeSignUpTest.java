package com.skillup.automation.signUp.negative;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pages.SignUpPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NegativeSignUpTest extends TestRunner {
    private static final String EXPECTED_EMAIL_ERROR_MESSAGE = "Please enter your email address";

    private SignUpPage signUpPage;

    @BeforeMethod
    public void before() {
        signUpPage = new SignUpPage(driver);
    }

    @Test
    public void wrongEmailTest() {

        signUpPage
                .open()
                .enterFirstName("First Name")
                .enterLastName("Last Name")
                .enterEmail("")
                .enterPassword("dwefefe")
                .clickJoinButton()
                .assertEmailErrorMessage(EXPECTED_EMAIL_ERROR_MESSAGE);

    }


}
