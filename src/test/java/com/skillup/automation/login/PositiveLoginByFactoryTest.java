package com.skillup.automation.login;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.LoginPageByFactory;
import org.testng.annotations.Test;

public class PositiveLoginByFactoryTest extends TestRunner {

    private static final String EXPECTED_LOGIN_URL = "https://www.linkedin.com/feed/";

    @Test
    public void positiveLoginTest() {
        LoginPageByFactory loginPage = new LoginPageByFactory(driver);


        loginPage
                .open()
                .enterEmail("iliashilkovit@gmail.com")
                .enterPassword("xboxlikeilia3535")
                .clickOnButton();


        onBoardingPage.assertIsOnPage(EXPECTED_LOGIN_URL);


    }

}
