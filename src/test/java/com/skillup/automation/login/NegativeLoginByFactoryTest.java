package com.skillup.automation.login;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.LoginPageByFactory;
import org.testng.annotations.Test;

import static com.skillup.automation.utils.RandomEmailString.getRandomEmail;

public class NegativeLoginByFactoryTest extends TestRunner {


    private static final String EXPECTED_ERROR_MESSAGE = "Please enter a password.";


    @Test
    public void test() {
        LoginPageByFactory loginPage = new LoginPageByFactory(driver);
        String email = getRandomEmail(10);


        loginPage
                .open()
                .enterEmail(email)
                .enterPassword("")
                .clickOnButton()
                .assertEmailErrorMessage(EXPECTED_ERROR_MESSAGE);
    }
}