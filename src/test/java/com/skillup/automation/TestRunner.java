package com.skillup.automation;

import com.skillup.automation.pages.OnBoardingPage;
import com.skillup.automation.pages.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class TestRunner {

    protected WebDriver driver;
    protected SignUpPage signUpPage;
    protected OnBoardingPage onBoardingPage;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite");
        WebDriverManager.chromedriver().setup();
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Before class");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Before method");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        signUpPage = new SignUpPage(driver);
        onBoardingPage = new OnBoardingPage(driver);
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("After method");
        driver.quit();
    }

    @AfterClass
    public void afterClass() {
        System.out.println("After class");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("After suite");
    }
}
