package com.skillup.automation.configuration;

public class Urls {
    public static final String LOGIN_URL = "https://www.linkedin.com/uas/login";
    public static final String SIGN_UP_URL = "https://www.linkedin.com/start/join";
}
