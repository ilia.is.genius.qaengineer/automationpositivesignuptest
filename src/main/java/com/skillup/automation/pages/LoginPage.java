package com.skillup.automation.pages;

import com.skillup.automation.utils.ElementHelper;
import com.skillup.automation.utils.LoginPageLocators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoginPage {
    private static final String EMAIL_OR_PHONE_INPUT_XPATH_LOCATOR = "//*[@name = 'session_key']";
    private static final String PASSWORD_INPUT_XPATH_LOCATOR = "//*[@name = 'session_password']";
    private static final String SHOW_SPAN_VISIBILITY_XPATH_LOCATOR = "//*[@class = 'button__password-visibility']";
    private static final String SIGH_IN_BUTTON_XPATH_LOCATOR = "//*[@class = 'login__form']//button";
    private static final String FORGOT_PASSWORD_LINK_XPATH_LOCATOR = "//a[contains(@href, 'request-password-reset')]";
    private static final String JOIN_NOW_LINK_XPATH_LOCATOR = "//*[@href = '/start/join']";
    private static final String USER_AGREEMENT_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'user-agreement')]";
    private static final String PRIVATE_POLICY_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'privacy-policy')]";
    private static final String COMMUNITY_GUIDELINES_LINK_XPATH_LOCATOR = "//a[contains(@href, 'community_guidelines')]";
    private static final String COOKIE_POLICY_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'cookie-policy')]";
    private static final String COPYRIGHT_POLICY_LINK_XPATH_LOCATOR = "//a[contains(@href, 'copyright-policy')]";
    private static final String SEND_FEEDBACK_LINK_XPATH_LOCATOR = "//a[contains(@href, 'send_feedback')]";
    private static final String LOGIN_URL_PAGE = "https://www.linkedin.com/uas/login";

    private ElementHelper helper = new ElementHelper();
    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(LOGIN_URL_PAGE);
    }

    public void open(String url) {
        driver.get(url);
    }


    public void enterEmail(String email) {
        WebElement input = driver.findElement(By.xpath(EMAIL_OR_PHONE_INPUT_XPATH_LOCATOR));
        helper.enterText(input, email);
    }

    public void enterPassword(String password) {
        WebElement input = driver.findElement(By.xpath(PASSWORD_INPUT_XPATH_LOCATOR));
        helper.enterText(input, password);
    }

    public void clickOnButton() {
        WebElement button = driver.findElement(By.xpath(SIGH_IN_BUTTON_XPATH_LOCATOR));
        button.click();
    }

    public void clickOnShowPassLink() {
        WebElement link = driver.findElement(By.xpath(SHOW_SPAN_VISIBILITY_XPATH_LOCATOR));
        link.click();
    }

    public String emailAlertMessage() {
        WebElement output = driver.findElement(By.cssSelector(LoginPageLocators.EMAIL_ALERT_MESSAGE_CSS_LOCATOR));
        return output.getText();
    }

    public String passwordAlertMessage() {
        WebElement output = driver.findElement(By.cssSelector(LoginPageLocators.PASSWORD_ALERT_MESSAGE_CSS_LOCATOR));
        return output.getText();
    }

    public void assertEmailErrorMessage(String expectedErrorMessage) {
        WebElement alertMessage = driver.findElement(By.cssSelector(LoginPageLocators.EMAIL_ALERT_MESSAGE_CSS_LOCATOR));
        String actualErrorMessage = alertMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Alert Message is incorrect");
    }

    public void assertPasswordErrorMessage(String expectedErrorMessage) {
        WebElement alertMessage = driver.findElement(By.cssSelector(LoginPageLocators.PASSWORD_ALERT_MESSAGE_CSS_LOCATOR));
        String actualErrorMessage = alertMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Alert Message is incorrect");
    }
}
