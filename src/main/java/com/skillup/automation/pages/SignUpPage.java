package com.skillup.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class SignUpPage extends CommonPage {
    private static final String FIRST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'firstName']";
    private static final String LAST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'lastName']";
    private static final String EMAIL_INPUT_XPATH_LOCATOR = "//*[@name = 'emailAddress']";
    private static final String PASSWORD_INPUT_XPATH_LOCATOR = "//*[@type = 'password']";
    private static final String AGREE_JOIN_BUTTON_XPATH_LOCATOR = "//*[contains(@class, 'join-btn')]";
    private static final String ERROR_ALERT_MESSAGE_CSS_LOCATOR = ".uno-alert strong";

    private static final String SignUp_URL = "https://www.linkedin.com/start/join";

    private String stringEndMail = "@gmail.com";

    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    public SignUpPage open() {
        driver.get(SignUp_URL);
        return this;
    }

    public SignUpPage enterFirstName(String firstName) {
        enterText(FIRST_NAME_INPUT_XPATH_LOCATOR, firstName);
        return this;
    }

    public SignUpPage enterLastName(String lastName) {
        enterText(LAST_NAME_INPUT_XPATH_LOCATOR, lastName);
        return this;
    }

    public SignUpPage enterEmail(String email) {
        enterText(EMAIL_INPUT_XPATH_LOCATOR, email);
        return this;
    }

    public SignUpPage enterPassword(String password) {
        enterText(PASSWORD_INPUT_XPATH_LOCATOR, password);
        return this;
    }

    public SignUpPage clickJoinButton() {
        click(AGREE_JOIN_BUTTON_XPATH_LOCATOR);
        return this;
    }

    public SignUpPage assertEmailErrorMessage(String expectedErrorMessage) {
        WebElement errorMessage = driver.findElement(By.cssSelector(ERROR_ALERT_MESSAGE_CSS_LOCATOR));
        String actualErrorMessage = errorMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message is wrong or not use!");
        return this;

    }




}
