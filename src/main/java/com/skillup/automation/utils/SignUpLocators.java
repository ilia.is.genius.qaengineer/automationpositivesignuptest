package com.skillup.automation.utils;

public class SignUpLocators {
    //First Name Input
    public static final String FIRST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'firstName']";
    public static final String FIRST_NAME_INPUT_CSS_LOCATOR = "#first-name";

    //Last Name Input
    public static final String LAST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'lastName']";
    public static final String LAST_NAME_INPUT_CSS_LOCATOR = "#last-name";

    //Email Input
    public static final String EMAIL_INPUT_XPATH_LOCATOR = "//*[@name = 'emailAddress']";
    public static final String EMAIL_INPUT_CSS_LOCATOR = "#join-email";

    //Password Input
    public static final String PASSWORD_INPUT_XPATH_LOCATOR = "//*[@type = 'password']";
    public static final String PASSWORD_INPUT_CSS_LOCATOR = "#join-password";

    //User Agreement Link
    public static final String USER_AGREEMENT_LINK_XPATH_LOCATOR = "//a[contains(@href, 'user-agreement')]";
    public static final String USER_AGREEMENT_LINK_INPUT_CSS_LOCATOR = "a[href *= 'user-agreement']";

    //Privacy Policy Link
    public static final String PRIVATE_POLICY_LINK_XPATH_LOCATOR = "//a[contains(@href, 'privacy-policy')]";
    public static final String PRIVATE_POLICY_LINK_CSS_LOCATOR = "a[href *= 'privacy-policy']";

    //Cookie Policy Link
    public static final String COOKIE_POLICY_LINK_XPATH_LOCATOR = "//a[contains(href, 'cookie-policy')]";
    public static final String COOKIE_POLICY_LINK_CSS_LOCATOR = "a[href *= 'cookie-policy']";

    //Agree&join Button
    public static final String AGREE_JOIN_BUTTON_XPATH_LOCATOR = "//*[contains(@class, 'join-btn')]";
    public static final String AGREE_JOIN_BUTTON_CSS_LOCATOR = ".join-btn";

    //Sigh In Link
    public static final String SIGH_IN_LINK_XPATH_LOCATOR = "//a[contains(@href, 'join-sign-in')]";
    public static final String SIGN_IN_LINK_CSS_LOCATOR = "a[href *= 'join-sign-in']";
}
