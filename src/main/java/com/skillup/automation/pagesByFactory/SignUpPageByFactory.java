package com.skillup.automation.pagesByFactory;

import com.skillup.automation.utils.ElementHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static com.skillup.automation.configuration.Urls.SIGN_UP_URL;

public class SignUpPageByFactory {


    @FindBy(xpath = "//*[@name = 'firstName']")
    private WebElement firstNameInput;

    @FindBy(xpath = "//*[@name = 'lastName']")
    private WebElement lastNameInput;

    @FindBy(xpath = "//*[@name = 'emailAddress']")
    private WebElement emailInput;

    @FindBy(xpath = "//*[@type = 'password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[contains(@class, 'join-btn')]")
    private WebElement signUpButton;

    @FindBy(css = ".uno-alert strong")
    private WebElement errorForMessage;

    private WebDriver driver;
    private ElementHelper helper = new ElementHelper();

    public SignUpPageByFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public SignUpPageByFactory open() {
        driver.get(SIGN_UP_URL);
        return this;
    }

    public SignUpPageByFactory enterFirstName(String firstName) {
        helper.enterText(firstNameInput, firstName);
        return this;
    }

    public SignUpPageByFactory enterLastName(String lastName) {
        helper.enterText(lastNameInput, lastName);
        return this;
    }

    public SignUpPageByFactory enterEmail(String email) {
        helper.enterText(emailInput, email);
        return this;
    }

    public SignUpPageByFactory enterPassword(String password) {
        helper.enterText(passwordInput, password);
        return this;
    }

    public SignUpPageByFactory clickSignUpButton() {
        signUpButton.click();
        return this;
    }

    public SignUpPageByFactory assertEmailErrorMessage(String expectedErrorMessage) {
        String actualErrorMessage = errorForMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message is wrong or not use!");
        return this;

    }
}
